# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ROOT as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( ROOT )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_ROOT )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building ROOT as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get ROOT from.
set( ATLAS_ROOT_SOURCE
   "URL;http://root.cern.ch/download/root_v6.26.08.source.tar.gz;URL_MD5;0d35d3896b865c9d294146be346d5530"
   CACHE STRING "The source for ROOT" )
mark_as_advanced( ATLAS_ROOT_SOURCE )

# Decide whether / how to patch the ROOT sources.
set( ATLAS_ROOT_PATCH
   "PATCH_COMMAND;patch;-p1;<;${CMAKE_CURRENT_SOURCE_DIR}/patches/v6-26-08-vdt.patch;COMMAND;patch;-p1;<;${CMAKE_CURRENT_SOURCE_DIR}/patches/v6-26-08-cmp0135.patch"
   CACHE STRING "Patch command for ROOT" )
set( ATLAS_ROOT_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of ROOT (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_ROOT_PATCH ATLAS_ROOT_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ROOTBuild" )

# Extra arguments for the CMake configuration of the ROOT build:
set( _extraArgs )
set( _cmakePrefixes )
if( APPLE )
   list( APPEND _extraArgs -Dvc:BOOL=OFF -Drpath:BOOL=ON )
endif()
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Set the C++ standard for the build.
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS} )
endif()

# The build needs Python:
if( NOT ATLAS_BUILD_PYTHON )
   find_package( Python COMPONENTS Interpreter Development REQUIRED )
   find_package( libffi REQUIRED )
endif()
list( APPEND _extraArgs
   -DPYTHON_EXECUTABLE:FILEPATH=$<TARGET_FILE:Python::Interpreter>
   -DPython3_EXECUTABLE:FILEPATH=$<TARGET_FILE:Python::Interpreter>
   -DPython3_INCLUDE_DIR:PATH=$<TARGET_PROPERTY:Python::Python,INTERFACE_INCLUDE_DIRECTORIES>
   -DPython3_LIBRARY:FILEPATH=$<TARGET_FILE:Python::Python> )

# ...and TBB.
if( NOT ATLAS_BUILD_TBB )
   find_package( TBB REQUIRED )
endif()
list( APPEND _extraArgs
   -DTBB_ROOT_DIR:PATH=$<TARGET_PROPERTY:TBB::tbb,INSTALL_PATH> )

# ...and XRootD.
if( NOT ATLAS_BUILD_XROOTD )
   find_package( Xrootd REQUIRED )
endif()
list( APPEND _extraArgs
   -Dxrootd:BOOL=ON
   -DXROOTD_ROOT_DIR:PATH=$<TARGET_PROPERTY:Xrootd::Utils,INSTALL_PATH> )

# ...and optionally DCAP.
if( NOT ATLAS_BUILD_DCAP )
   find_package( dcap )
endif()
if( ATLAS_BUILD_DCAP OR DCAP_FOUND )
   list( APPEND _extraArgs
      -Ddcache:BOOL=ON
      -DDCAP_DIR:PATH=$<TARGET_PROPERTY:dcap::dcap,INSTALL_PATH> )
endif()

# ...and optionally Davix.
if( NOT ATLAS_BUILD_DAVIX )
   find_package( Davix )
endif()
if( ATLAS_BUILD_DAVIX OR DAVIX_FOUND )
   list( APPEND _extraArgs -Ddavix:BOOL=ON )
   list( APPEND _cmakePrefixes $<TARGET_PROPERTY:Davix::davix,INSTALL_PATH> )
endif()

# ...and optionally LibXml2.
if( NOT ATLAS_BUILD_LIBXML2 )
   find_package( LibXml2 )
endif()
if( ATLAS_BUILD_LIBXML2 OR LibXml2_FOUND )
   list( APPEND _extraArgs
      -Dxml:BOOL=ON
      -DLIBXML2_INCLUDE_DIR:PATH=$<TARGET_PROPERTY:LibXml2::LibXml2,INTERFACE_INCLUDE_DIRECTORIES>
      -DLIBXML2_LIBRARY:FILEPATH=$<TARGET_FILE:LibXml2::LibXml2> )
endif()

# ...and optionally PyAnalysis.
if( ATLAS_BUILD_PYANALYSIS )
   list( APPEND _cmakePrefixes
      $<TARGET_PROPERTY:PyAnalysis::PyAnalysis,INSTALL_PATH> )
endif()

# Handle all the externals that need to be set up through CMAKE_PREFIX_PATH.
if( _cmakePrefixes )
   string( REPLACE ";" "%" _cmakePrefixes "${_cmakePrefixes}" )
   list( APPEND _extraArgs
      -DCMAKE_PREFIX_PATH:PATH="${_cmakePrefixes}" )
endif()

# ...and optionally FFTW3.
if( "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" OR
    "${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "i686" )
   list( APPEND _extraArgs -Dbuiltin_fftw3:BOOL=ON )
endif()

# Make it possible to build LLVM/Cling in a non-Release mode as well. Which can
# come in handy for debugging dictionary issues. However this should only be
# modified for local builds, for debugging. (The size of ROOT increases
# drastically, and it becomes quite a bit slower with this turned to "Debug".)
set( ATLAS_ROOT_LLVM_BUILD_TYPE "Release" CACHE STRING
   "Build type used for ROOT's LLVM/Cling" )
mark_as_advanced( ATLAS_ROOT_LLVM_BUILD_TYPE )
list( APPEND _extraArgs
   -DLLVM_BUILD_TYPE:STRING="${ATLAS_ROOT_LLVM_BUILD_TYPE}" )

# Generate the script that will configure the build of ROOT.
string( REPLACE ";" " " _configureArgs "${_extraArgs}" )
string( REPLACE "%" ";" _configureArgs "${_configureArgs}" )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure_pregen.sh"
   @ONLY )
file( GENERATE
   OUTPUT "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   INPUT "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure_pregen.sh" )
unset( _configureArgs )

# Build ROOT:
ExternalProject_Add( ROOT
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_ROOT_SOURCE}
   ${ATLAS_ROOT_PATCH}
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( ROOT forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_ROOT_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( ROOT purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for ROOT"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( ROOT forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of ROOT"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( ROOT buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing ROOT into the build area"
   DEPENDEES install )
add_dependencies( Package_ROOT ROOT )
if( ATLAS_BUILD_PYANALYSIS )
   add_dependencies( ROOT Package_PyAnalysis )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( ROOT Python )
endif()
if( ATLAS_BUILD_TBB )
   add_dependencies( ROOT TBB )
endif()
if( ATLAS_BUILD_XROOTD )
   add_dependencies( ROOT XRootD )
endif()
if( ATLAS_BUILD_DCAP )
   add_dependencies( ROOT dcap )
endif()
if( ATLAS_BUILD_DAVIX )
   add_dependencies( ROOT Davix )
endif()
if( ATLAS_BUILD_LIBXML2 )
   add_dependencies( ROOT LibXml2 )
endif()

# Install ROOT:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
