// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#include "check_ts1_test1.h"
#include "Control/p1/p1/p1a.h"
#include "Control/p1/p1/p1b.h"
#include "Control/p2/p2/p2a.h"
#include "Control/p2/p2/p2b.h"

void f1 [[ATLAS::check_thread_safety_debug]] () {}
void f2 [[ATLAS::check_thread_safety_debug]] [[ATLAS::check_thread_safety]] () {}


struct [[ATLAS::check_thread_safety_debug]] C1
{
  void g1 [[ATLAS::check_thread_safety_debug]] () {}
  void g2 [[ATLAS::check_thread_safety_debug]] ();
  void g3();
  void g4 [[ATLAS::check_thread_safety]] [[ATLAS::check_thread_safety_debug]] ();
  void g5 [[ATLAS::not_thread_safe]] [[ATLAS::check_thread_safety_debug]] ();
};


void C1::g2() {}
void C1::g3 [[ATLAS::check_thread_safety_debug]] () {}
void C1::g4() {}


struct [[ATLAS::check_thread_safety]] C2
{
  void h1 [[ATLAS::check_thread_safety_debug]] () {}
  void h2();

  struct C3 {
    void h3();
  };

  void h4 [[ATLAS::not_thread_safe]] [[ATLAS::check_thread_safety_debug]] () {}
};

void C2::h2 [[ATLAS::check_thread_safety_debug]] () {}
void C2::C3::h3 [[ATLAS::check_thread_safety_debug]] () {}


