// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
// Test callcheck for cond/meta Handle classes.


class EventContext {};
class EventIDRange {};
class StatusCode {};

namespace Gaudi {
namespace Hive {
const EventContext& currentContext();
}
}


namespace SG {


template <class T>  class ReadCondHandleKey {};


template <typename T>
class ReadCondHandle
{
public:
  ReadCondHandle(const SG::ReadCondHandleKey<T>& key);
  ReadCondHandle(const SG::ReadCondHandleKey<T>& key, 
                 const EventContext& ctx);
};


//*************************************************************************


template <class T>  class WriteCondHandleKey {};


template <typename T>
class WriteCondHandle
{
public:
  WriteCondHandle(const WriteCondHandleKey<T>& key);
  WriteCondHandle(const WriteCondHandleKey<T>& key, const EventContext& ctx);

  StatusCode extendLastRange(const EventIDRange& range,
                             const EventContext& ctx = Gaudi::Hive::currentContext());
};


//*************************************************************************

template <class T>  class ReadMetaHandleKey {};


template <class T>
class ReadMetaHandle
{
public:
  ReadMetaHandle(const ReadMetaHandleKey<T>& key);
  ReadMetaHandle(const ReadMetaHandleKey<T>& key, const EventContext& ctx);
};


//*************************************************************************

template <class T>  class WriteMetaHandleKey {};


template <class T>
class WriteMetaHandle
{
public:
  WriteMetaHandle(const WriteMetaHandleKey<T>& key);
  WriteMetaHandle(const WriteMetaHandleKey<T>& key, const EventContext& ctx);
};


}


void testReadCondHandle (const SG::ReadCondHandleKey<int>& k,
                         const EventContext& xyzzy)
{
  SG::ReadCondHandle<int> h2 (k, xyzzy);
  SG::ReadCondHandle<int> h (k);
}


void testWriteCondHandle (const SG::WriteCondHandleKey<int>& k,
                          const EventIDRange& range,
                          const EventContext& xyzzy)
{
  SG::WriteCondHandle<int> h2 (k, xyzzy);
  SG::WriteCondHandle<int> h (k);
  h.extendLastRange (range);
  h.extendLastRange (range, xyzzy);
}


void testReadMetaHandle (const SG::ReadMetaHandleKey<int>& k,
                         const EventContext& xyzzy)
{
  SG::ReadMetaHandle<int> h2 (k, xyzzy);
  SG::ReadMetaHandle<int> h (k);
}


void testWriteMetaHandle (const SG::WriteMetaHandleKey<int>& k,
                            const EventContext& xyzzy)
{
  SG::WriteMetaHandle<int> h2 (k, xyzzy);
  SG::WriteMetaHandle<int> h (k);
}
