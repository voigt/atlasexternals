# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building ONNX Runtime as part of the offline / analysis
# release.
#

# This package needs CMake 3.18 at least.
cmake_minimum_required( VERSION 3.18 )

# Set the name of the package:
atlas_subdir( onnxruntime )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get ONNXRuntime from.
set( ATLAS_ONNXRUNTIME_SOURCE
   "URL;http://cern.ch/atlas-software-dist-eos/externals/onnxruntime/onnxruntime-v1.15.1.tar.bz2;URL_MD5;12c68a97e758d669f7cafee4b908a2b7"
   CACHE STRING "The source for ONNXRuntime" )
mark_as_advanced( ATLAS_ONNXRUNTIME_SOURCE )

# Decide whether / how to patch the ONNXRuntime sources.
set( ATLAS_ONNXRUNTIME_PATCH
     "PATCH_COMMAND;patch;-p1;<;${CMAKE_CURRENT_SOURCE_DIR}/patches/flatbuffer-v1.12.0.patch"
     CACHE STRING "Patch command for ONNXRuntime" )
set( ATLAS_ONNXRUNTIME_PATCH "" CACHE STRING "Patch command for ONNXRuntime" )
set( ATLAS_ONNXRUNTIME_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of ONNXRuntime (2023.07.21.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_ONNXRUNTIME_PATCH
   ATLAS_ONNXRUNTIME_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/onnxruntimeBuild" )

# Extra arguments for the build configuration:
set( _extraArgs )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Find all necessary externals.
if( ATLAS_BUILD_PYTHON )
   set( Python_EXECUTABLE "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/python3" )
else()
   find_package( Python COMPONENTS Interpreter )
endif()
find_package( ZLIB )
find_package( PNG )

# Find the optional externals.
option( ATLAS_ONNXRUNTIME_USE_CUDA
   "Use the CUDA capabilities of ONNX Runtime, if possible" TRUE )
mark_as_advanced( ATLAS_ONNXRUNTIME_USE_CUDA )
if( ATLAS_ONNXRUNTIME_USE_CUDA )
   find_package( CUDAToolkit )
   find_package( cuDNN )
   if( CUDAToolkit_FOUND AND CUDNN_FOUND )
      message( STATUS "Building onnxruntime with CUDA support" )
      list( APPEND _extraArgs
         -DCMAKE_PREFIX_PATH:PATH=${CUDAToolkit_LIBRARY_ROOT}
         -Donnxruntime_USE_CUDA:BOOL=TRUE
         -Donnxruntime_CUDA_HOME:STRING=${CUDAToolkit_LIBRARY_ROOT}
         -Donnxruntime_CUDNN_HOME:STRING=$<TARGET_PROPERTY:cuDNN::cudnn,INSTALL_PATH> )
      if( NOT "${CMAKE_CUDA_ARCHITECTURES}" STREQUAL "" )
         list( APPEND _extraArgs
            -DCMAKE_CUDA_ARCHITECTURES:STRING=${CMAKE_CUDA_ARCHITECTURES} )
      endif()
   endif()
endif()

# Set up the custom "cmake script".
if( ("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU") )
   set( _cmakeScript
      "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/onnxruntime_cmake.sh" )
   configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/onnxruntime_cmake.sh.in"
      "${_cmakeScript}" @ONLY )
endif()

# Build onnxruntime.
ExternalProject_Add( onnxruntime
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_ONNXRUNTIME_SOURCE}
   ${ATLAS_ONNXRUNTIME_PATCH}
   SOURCE_SUBDIR "cmake"
   CMAKE_COMMAND "${_cmakeScript}"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -Donnxruntime_BUILD_SHARED_LIB:BOOL=ON
   -Donnxruntime_BUILD_UNIT_TESTS:BOOL=OFF
   -DPYTHON_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
   -DPython_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
   -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIR}
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY}
   -DPNG_PNG_INCLUDE_DIR:PATH=${PNG_PNG_INCLUDE_DIR}
   -DPNG_LIBRARY:FILEPATH=${PNG_LIBRARY}
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( onnxruntime forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "${ATLAS_ONNXRUNTIME_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( onnxruntime purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for onnxruntime."
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( onnxruntime forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of onnxruntime"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( onnxruntime buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing onnxruntime into the build area"
   DEPENDEES install )
add_dependencies( Package_onnxruntime onnxruntime )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( onnxruntime Python )
endif()

# Install onnxruntime.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
install( FILES "cmake/Findonnxruntime.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
