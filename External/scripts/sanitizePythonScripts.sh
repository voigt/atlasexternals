#!/bin/bash
#
# Script used to sanitize the executable scripts after their installation,
# to make them relocatable.
#
# Workaround for https://github.com/colcon/colcon-bundle/issues/6
#

# Fail on errors:
set -e
# globs matching no files evaluate to empty string:
shopt -s nullglob

# Loop over all scripts provided on the command line:
for script in $*; do

    # Create a sanitized version of the script, by just replacing
    # its first line with a relocatable expression:
    sed -i -e '1s:#!.*:#!/usr/bin/env python:' ${script}
done
