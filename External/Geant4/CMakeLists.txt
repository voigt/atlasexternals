# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building Geant4 with all its ATLAS specific patches, for the offline
# builds.
#

# The name of the package:
atlas_subdir( Geant4 )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# The externals needed for the build. Note that we could actually
# skip finding the packages here. We just look for them already here
# to ease debugging in case they can't be found later on by Geant4 itself.
# And to set up a proper RPM dependency on them...
find_package( XercesC )
find_package( EXPAT )

# Declare where to get Geant4 from.
set( ATLAS_GEANT4_SOURCE
   "URL;https://gitlab.cern.ch/atlas-simulation-team/geant4/-/archive/v10.6.3.7/geant4-v10.6.3.7.tar.gz;URL_MD5;73ecf603ff07327f7cfa5601ffc17ec0"
   CACHE STRING "The source for Geant4" )
mark_as_advanced( ATLAS_GEANT4_SOURCE )

# Decide whether / how to patch the Geant4 sources.
set( ATLAS_G4VERS_ORIG "geant4.10.6.patch03.atlas07" )
set( ATLAS_G4VERS_PATCH "geant4.10.6.patch03.atlasmt07" )
set( ATLAS_GEANT4_PATCH "PATCH_COMMAND;sed;-i;s/${ATLAS_G4VERS_ORIG}/${ATLAS_G4VERS_PATCH}/g;<SOURCE_DIR>/source/global/management/include/G4Version.hh;<SOURCE_DIR>/source/run/src/G4RunManagerKernel.cc;COMMAND;patch;-p1;<;${CMAKE_CURRENT_SOURCE_DIR}/patches/geant4-vecgeom-1.1.20.patch;COMMAND;patch;-p1;<;${CMAKE_CURRENT_SOURCE_DIR}/patches/geant4-cpp20.patch"
   CACHE STRING "Patch command for Geant4" )
set( ATLAS_GEANT4_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Geant4 (2023.06.27.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_GEANT4_PATCH ATLAS_GEANT4_FORCEDOWNLOAD_MESSAGE )

# Extra options for the configuration:
set( _extraOptions )
set( _cmakePrefixes )

if( "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++11 )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 14 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++14 )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 17 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++17 )
endif()
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Expat from LCG
list( APPEND _cmakePrefixes "${EXPAT_LCGROOT}" )

# VecGeom from AtlasExternals
list( APPEND _cmakePrefixes "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}")

# Handle all the externals that need to be set up through CMAKE_PREFIX_PATH.
if( _cmakePrefixes )
   list( REMOVE_DUPLICATES _cmakePrefixes )
   list( APPEND _extraOptions 
       -DCMAKE_PREFIX_PATH:PATH=${_cmakePrefixes} )
endif()

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Geant4Build" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Geant4Stamp" )

# Create the script that will sanitize the geant4-config script after the build:
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   @ONLY )

# Build Geant4 for the build area:
ExternalProject_Add( Geant4
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   ${ATLAS_GEANT4_SOURCE}
   ${ATLAS_GEANT4_PATCH}
   CMAKE_CACHE_ARGS
   -DGEANT4_USE_GDML:BOOL=ON
   -DGEANT4_USE_SYSTEM_ZLIB:BOOL=OFF
   -DCLHEP_ROOT_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DXERCESC_ROOT_DIR:PATH=${XERCESC_LCGROOT}
   -DGEANT4_USE_SYSTEM_CLHEP:BOOL=ON
   -DGEANT4_USE_SYSTEM_CLHEP_GRANULAR:BOOL=OFF
   -DGEANT4_BUILD_MULTITHREADED:BOOL=ON
   -DGEANT4_BUILD_TLS_MODEL:STRING=global-dynamic
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:STRING=lib
   -DGEANT4_INSTALL_DATA:BOOL=OFF
   -DGEANT4_INSTALL_PACKAGE_CACHE:BOOL=OFF
   -DGEANT4_USE_USOLIDS:STRING=CONS;POLYCONE
   -DBUILD_SHARED_LIBS:BOOL=OFF
   -DBUILD_STATIC_LIBS:BOOL=ON
   -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Geant4 forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_GEANT4_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Geant4 purgebuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Geant4"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Geant4 forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of Geant4"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Geant4 buildinstall
   COMMAND "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/lib/Geant4-*/Linux-*"   
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Geant4 into the build area"
   DEPENDEES install )
add_dependencies( Package_Geant4 Geant4 )
add_dependencies( Geant4 CLHEP )
add_dependencies( Geant4 VecGeom )

# Install Geant4:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/FindGeant4.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
