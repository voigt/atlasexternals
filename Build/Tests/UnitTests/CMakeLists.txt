# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration

atlas_subdir( UnitTests )

#
# Tests for unit_test_executor.sh
#
atlas_add_test( utexec_success
   SCRIPT "exit 0"
   LABELS "Successful" )

atlas_add_test( utexec_fail
   SCRIPT "exit 1"
   PROPERTIES WILL_FAIL TRUE
   LABELS "Failure" )

atlas_add_test( utexec_fail_dummypost
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "# no post-processing"
   PROPERTIES WILL_FAIL TRUE
   LABELS "Successful" "Failure" )

atlas_add_test( utexec_success_dummypost
   SCRIPT "exit 0"
   POST_EXEC_SCRIPT "# no post-processing" )

atlas_add_test( utexec_testStatus
   SCRIPT "exit 42"
   POST_EXEC_SCRIPT "echo \${testStatus}"
   PROPERTIES PASS_REGULAR_EXPRESSION "42" )

atlas_add_test( utexec_success_post_fail
   SCRIPT "exit 0"
   POST_EXEC_SCRIPT "exit 1"
   PROPERTIES WILL_FAIL TRUE )

atlas_add_test( utexec_fail_post_success
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "exit 0" )

atlas_add_test( utexec_fail_nopost
   SCRIPT "exit 1"
   POST_EXEC_SCRIPT "exit \${testStatus}"
   PROPERTIES WILL_FAIL TRUE )

atlas_add_test( utexec_preexec
   SCRIPT "exit 0"
   PRE_EXEC_SCRIPT "echo Hello"
   PROPERTIES PASS_REGULAR_EXPRESSION "Hello" )

atlas_add_test( utexec_testname
   SCRIPT "test \${ATLAS_CTEST_PACKAGE} = UnitTests -a "
          "\${ATLAS_CTEST_TESTNAME} = utexec_testname " )

atlas_add_test( utexec_logpattern
   SCRIPT "test \${ATLAS_CTEST_LOG_SELECT_PATTERN} = '^.*pat1$' -a "
          "\${ATLAS_CTEST_LOG_IGNORE_PATTERN} = 'pat2.*|pat3'"
   LOG_SELECT_PATTERN "^.*pat1$"
   LOG_IGNORE_PATTERN "pat2.*|pat3" )

atlas_add_test( utexec_environment
   SCRIPT "echo -n \${FOO} && echo -n \${BAR}"
   ENVIRONMENT FOO=foo BAR=bar
   POST_EXEC_SCRIPT "logContent=`cat utexec_environment.log`
if [[ \"$logContent\" != \"foobar\" ]]
then
   exit 1
fi" )

# Test dependency tests:
atlas_add_test( test_depends1
   # sleep to check if depends2 really runs afterwards
   SCRIPT "sleep 1 && echo 1" > test_depends.log )

atlas_add_test( test_depends2
   SCRIPT "echo 2" >> test_depends.log
   DEPENDS test_depends1 )

atlas_add_test( test_depends3
   SCRIPT "echo 3" >> test_depends.log
   DEPENDS test_depends1 test_depends2
   POST_EXEC_SCRIPT "echo '1\n2\n3' | diff test_depends.log -" )
