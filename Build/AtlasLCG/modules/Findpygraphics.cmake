# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYGRAPHICS_FOUND
#  PYGRAPHICS_PYTHON_PATH
#  PYGRAPHICS_BINARY_PATH
#  PYGRAPHICS_pyrcc4_EXECUTABLE
#  PYGRAPHICS_pyrcc5_EXECUTABLE
#
# Can be steered by PYGRAPHICS_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME pygraphics
   PYTHON_NAMES pydot.py PyQt4/__init__.py PyQt5/__init__.py
   BINARY_NAMES pyrcc4 pyrcc5
   BINARY_SUFFIXES bin )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pygraphics DEFAULT_MSG
   _PYGRAPHICS_PYTHON_PATH _PYGRAPHICS_BINARY_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pygraphics )
