# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# The version is just a dummy.
set( PACKAGE_VERSION "1.0.0" )

# Don't do any version checks for this dummy version.
set( PACKAGE_VERSION_COMPATIBLE TRUE )
set( PACKAGE_VERSION_EXACT TRUE )
