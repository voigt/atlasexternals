# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration options for building the Athena externals. Collected
# into a single place.
#

# Look for appropriate externals.
find_package( nlohmann_json QUIET )
find_package( SYCL QUIET )

# Decide whether to build nlohmann_json.
set( _flag FALSE )
if( NOT nlohmann_json_FOUND )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_NLOHMANN_JSON
   "Build nlohmann_json as part of the release" ${_flag} )

# Decide whether to build triSYCL.
set( _flag FALSE )
if( NOT SYCL_FOUND )
   set( _flag TRUE )
endif()
option( ATLAS_BUILD_TRISYCL "Build triSYCL as part of this project" ${_flag} )

# Default build options.
option( ATLAS_BUILD_FASTJET
   "Build FastJet/FJContrib as part of this project" ON )
option( ATLAS_BUILD_CLHEP
   "Build CLHEP as part of this project" ON )
option( ATLAS_BUILD_CORAL
   "Build CORAL as part of this project" OFF )
option( ATLAS_BUILD_COOL
   "Build COOL as part of this project" OFF )

# Make CMake forget that it found any of these packages. (In case it did.)
# Since they could interfere with the environment setup of the project.
# Whichever package ends up needing those externals, will anyway ask for
# them explicitly.
get_property( _packages GLOBAL PROPERTY PACKAGES_FOUND )
list( REMOVE_ITEM _packages nlohmann_json SYCL )
set_property( GLOBAL PROPERTY PACKAGES_FOUND ${_packages} )
unset( _packages )

# Clean up.
unset( _flag )
