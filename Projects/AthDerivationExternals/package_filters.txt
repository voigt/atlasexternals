# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthDerivationExternals.
#
+ External/Acts
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/Gaudi
+ External/GeoModel
+ External/Gdb
+ External/GPerfTools
+ External/GoogleTest
+ External/MKL
+ External/dSFMT
+ External/lwtnn
+ External/onnxruntime
+ External/prmon
+ External/yampl
+ External/Lhapdf
+ External/nlohmann_json
- .*
