# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# GitLab-CI configuration for the repository.
#

# Stages for the CI build.
stages:
  - build

# Job-wide variables.
variables:
  CENTOS_PACKAGES: which wget tar atlas-devel redhat-lsb-core libX11-devel libXpm-devel libXft-devel libXext-devel libXi-devel openssl-devel glibc-devel gmp-devel rpm-build mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel
  UBUNTU_PACKAGES: git wget lsb-core libx11-dev libxpm-dev libxft-dev libxext-dev libxi-dev libssl-dev libgmp-dev libgl1-mesa-dev libglu1-mesa-dev libegl1-mesa-dev
  CMAKE_VERSION: 3.26.4
  GIT_VERSION: default

# Template for all build jobs.
.build_template: &build_job
  stage: build
  script:
    - cmake -DCTEST_USE_LAUNCHERS=TRUE ${CMAKE_ARGUMENTS} -S Projects/${PROJECT_NAME} -B ci_build 2>&1 | tee cmake_config.log
    - cmake --build ci_build 2>&1 | tee cmake_build.log
    - ctest --test-dir ci_build -j `nproc` --output-on-failure --output-junit testresults.xml 2>&1 | tee cmake_test.log
  only:
    - main
    - merge_requests
  artifacts:
    when: always
    reports:
      junit: ci_build/testresults.xml
    name: atlasexternals-${PROJECT_NAME}-job-logs-${CI_JOB_ID}
    expose_as: 'Job Logs'
    paths:
      - cmake_config.log
      - cmake_build.log
      - cmake_test.log
      - ci_build/BuildLogs/
    expire_in: 1 week

# Template for all x86_64-centos7-gcc11 jobs.
.x86_64_centos7_gcc11_template: &x86_64_centos7_gcc11_job
  tags:
    - docker
    - cvmfs
  image: centos:7
  before_script:
    - yum install -y ${CENTOS_PACKAGES}
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - set +e
    - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    - asetup none,centos7,gcc11
    - lsetup "cmake ${CMAKE_VERSION}"
    - lsetup "git ${GIT_VERSION}"
    - set -e
    - export LC_ALL=en_US.utf8

# Template for all x86_64-ubuntu2004-gcc9 jobs.
.x86_64_ubuntu2004_gcc9_template: &x86_64_ubuntu2004_gcc9_job
  tags:
    - docker
    - cvmfs
  image: ubuntu:20.04
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update
    - apt-get install -y ${UBUNTU_PACKAGES}
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - set +e
    - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    - lsetup "cmake ${CMAKE_VERSION}"
    - set -e
    - export MAKEFLAGS="-j`nproc` -l`nproc`"
    - export LANG=C.UTF-8
    - export LC_ALL=C.UTF-8

# Template for all aarch64-centos7-gcc9 jobs.
.aarch64_centos7_gcc9_template: &aarch64_centos7_gcc9_job
  tags:
    - docker-arm
  image: centos:7
  before_script:
    - yum install -y ${CENTOS_PACKAGES}
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - set +e
    - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    - asetup none,gcc9
    - lsetup "cmake ${CMAKE_VERSION}"
    - lsetup "git ${GIT_VERSION}"
    - set -e

#
# x86_64-centos7-gcc11 build(s).
#
build:x86_64-centos7-gcc11:
  <<: *build_job
  <<: *x86_64_centos7_gcc11_job
  parallel:
    matrix:
      - PROJECT_NAME: ['AnalysisBaseExternals', 'AthAnalysisExternals',
                       'AthDerivationExternals', 'AthenaExternals',
                       'AthGenerationExternals', 'AthSimulationExternals',
                       'TestExternals', 'VP1LightExternals']
        CMAKE_ARGUMENTS: ['-DCMAKE_BUILD_TYPE=RelWithDebInfo']

#
# x86_64-ubuntu2004-gcc9 build(s).
#
build:x86_64-ubuntu2004-gcc9:
  <<: *build_job
  <<: *x86_64_ubuntu2004_gcc9_job
  parallel:
    matrix:
      - PROJECT_NAME: ['AnalysisBaseExternals']
        CMAKE_ARGUMENTS: ['-DCMAKE_BUILD_TYPE=Release']
